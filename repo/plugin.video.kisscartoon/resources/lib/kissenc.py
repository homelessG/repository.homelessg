#!/usr/bin/env python
"""
Kissasian and Kisscaroon decryption
Created by: Twoure
Date:       06/04/2016
Changed to Work with Kodi KissCartoon/Asian Addon 06/05/2016 PrometheusX
"""
import binascii
import pyaes
from sha2 import sha256 as SHA256

import base64

class KissDecrypt:
    def __init__(self): 
        #kissasian
        di = base64.b64decode("XzMyYjgxMmU5YTEzMjFhZTBlODRhZjY2MGM0NzIyYjNhXw==")[1:-1]
        self.derived_drama_iv = binascii.a2b_hex(di)
        #kisscartoon
        ci = base64.b64decode("X2E1ZThkMmU5YzE3MjFhZTBlODRhZDY2MGM0NzJjMWYzXw==")[1:-1]
        self.derived_cartoon_iv = binascii.a2b_hex(ci)
        
    def unpad_string(self, text, k=16):
        """
        Remove the PKCS#7 padding from a text string
        Made by https://gist.github.com/chrix2
        """
        nl = len(text)
        val = int(binascii.b2a_hex(text[-1]), 16)
        if val > k:
            raise ValueError('Input is not padded or padding is corrupt')
        l = nl - val
        return text[:l]
               
    def ensure_unicode(self, v):
        if isinstance(v, str):
            v = v.decode('utf8')
        return unicode(v)

    def decrypt(self, f, kind, r):#url=None, dh=None):
        """
        decrypt video URL input depending on what site it came from
        """
        #headers.update({'X-Requested-With': 'XMLHttpRequest', 'Content-Length': '0'})
        #r = requests.post(Common.GetBaseURL(url) + '/External/RSK', headers=headers)
        key = str(r)
        derived_key = SHA256(key).digest()
        if kind == 'drama':
            return self.decrypt_input(f, derived_key, self.derived_drama_iv)
        else:
            return self.decrypt_input(f, derived_key, self.derived_cartoon_iv)       

    def decrypt_input(self, f, key, iv):
        """decrypt video URL"""
        aes = pyaes.AESModeOfOperationCBC(key, iv)
        etb = binascii.a2b_base64(f)

        if len(etb) >= 16:
            dt = []
            for i in xrange(len(etb)/16):
                d = aes.decrypt(etb[i*(16):(i+1)*(16)])
                dt.append(d)
            r = ''.join(dt)
            r = self.unpad_string(r)
            return self.ensure_unicode(r)
        else:
            Log.Error('* KissDecrypt Error: ciphertext block is not larger than 16')
            raise ValueError('ciphertext block is not larger than 16')